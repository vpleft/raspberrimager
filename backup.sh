#!/bin/bash

help_n_exit() {
    echo "Usage: $0 [OPTION]... [DEVICE] [FILE]"
    echo "Create backups of Raspberry PI SD Cards."
    echo "The backup will be in a gzip compressed img format."
    echo "If no FILE specified, will use the current datetime as"
    echo "output file name in the following format: \"%F_%H-%M.img.gz\""
    echo ""
    echo "Examples:"
    echo "    $0 /dev/sdX"
    echo "    $0 /dev/sdX ./backup.img.gz"
    echo "    $0 -y -z /dev/sdX ./backup.img.gz"
    echo ""
    echo "Options:"
    echo "  -z    Fill free space with zeroes on partitions."
    echo "        Helps to create much smaller images with the"
    echo "        used gzip compression."
    echo "  -y    Don't prompt for confirmation."
    echo "  -h    This help message."
    echo ""
    exit $1
}

FILL_WITH_ZEROES=0
NO_CONFIRMATION=0
while getopts "zyh" opt
do
    case "${opt}" in
      z)
        FILL_WITH_ZEROES=1
        ;;
      y)
        NO_CONFIRMATION=1
        ;;
      h)
        help_n_exit 0
        ;;
    esac
done
shift "$((OPTIND-1))" # Shift out option parameters (just device and output will remain)

if [[ $1 == "" ]]; then help_n_exit 1; fi
if [[ $EUID != 0 ]]; then
    echo "Please run this script with sudo!"
    exit 1
fi

# Check if parameters are valid
DEVICE="$1"
echo $DEVICE | grep -q -E "^/dev/[0-9A-Za-z]+$" || (echo "Error: Wrong device!" && help_n_exit 1)

OUTPUT="$2"
if [[ $OUTPUT == "" ]]; then
    OUTPUT=$(date +"%F_%H-%M.img.gz")
fi

# Check if the output file is accessible
touch $OUTPUT || (echo "Error: Wrong output file or it is not accessible!" && help_n_exit 1)
rm $OUTPUT

if [[ $FILL_WITH_ZEROES == 0 ]]; then echo "DON'T FILL free space on device with zeroes."
                                 else echo "FILL free space on device with zeroes."; fi
echo "Will make a backup of $DEVICE to $OUTPUT."
if [[ $NO_CONFIRMATION != 1 ]]; then
    read -p "Would you like to proceed? [Y/N] " ANSWER
    ANSWER=${ANSWER,,}
    if [[ $ANSWER != "y" && $ANSWER != "yes" ]]; then
        echo "Aborting."
        exit 1
    fi
fi

clear
if [[ $FILL_WITH_ZEROES == 1 ]]; then
    echo ""
    echo "Filling partitions with zeroes ('no space left on device' errors will be normal!)..."
    mkdir -p ./mkbackup/tmpmount
    PARTITIONS=$(fdisk -l | grep -E -o "$DEVICE[0-9]+" | sort --unique)
    for partition in $PARTITIONS; do
        echo "Mount and fill partition $partition"
        mount $partition ./mkbackup/tmpmount
        pv /dev/zero > ./mkbackup/tmpmount/DESTROYMEICANTTAKEITANYMORE.imabigzero

        # Sync and wait for buffers to flush
        # Maybe not effective and maybe not required but it can't hurt
        sync
        sleep 10s

        rm ./mkbackup/tmpmount/DESTROYMEICANTTAKEITANYMORE.imabigzero
        umount $partition
    done
    rm -rf ./mkbackup
fi


MOUNTS=$(mount | grep -E -o "$DEVICE[0-9]+" | sort --unique)
if [[ $MOUNTS != "" ]]; then
    echo ""
    echo "Umounting $MOUNTS..."
    umount $MOUNTS
    if [[ $? != 0 ]]; then
        echo "An error occured while trying to umount a partition."
        echo "Aborting."
        exit 1
    fi
fi

echo ""
echo "Making backup of $DEVICE to $OUTPUT..."
pv $DEVICE | gzip > $OUTPUT
user="$(logname)"
chown $user:$user $OUTPUT
echo "done."
