#!/bin/bash

help_n_exit() {
    echo "Usage: $0 [OPTION]... [FILE] [DEVICE]"
    echo "Restore Raspberry PI SD card to a previously created backup."
    echo "The backup must be in a gzip compressed img format."
    echo ""
    echo "Examples:"
    echo "    $0 ./backup.img.gz /dev/sdX"
    echo "    $0 -y ./backup.img.gz /dev/sdX"
    echo ""
    echo "Options:"
    echo "  -y    Don't prompt for confirmation."
    echo "  -h    This help message."
    echo ""
    exit $1
}

NO_CONFIRMATION=0
while getopts "yh" opt
do
    case "${opt}" in
      y)
        NO_CONFIRMATION=1
        ;;
      h)
        help_n_exit 0
        ;;
    esac
done
shift "$((OPTIND-1))" # Shift out option parameters (just device and output will remain)

if [[ $1 == "" || $2 == "" ]]; then help_n_exit 1; fi
if [[ $EUID != 0 ]]; then
    echo "Please run this script with sudo!"
    exit 1
fi

DEVICE=$2
echo $DEVICE | grep -q -E "^/dev/[0-9A-Za-z]+$" || (echo "Error: Wrong output device!" && help_n_exit 1)

FILE=$1
[[ -f $FILE ]] || (echo "Error: Input file does not exists!" && help_n_exit 1)

echo "Will restore $FILE to $DEVICE."
if [[ $NO_CONFIRMATION != 1 ]]; then
    read -p "Would you like to proceed? [Y/N] " ANSWER
    ANSWER=${ANSWER,,}
    if [[ $ANSWER != "y" && $ANSWER != "yes" ]]; then
        echo "Aborting."
        exit 1
    fi
fi

gunzip --stdout $FILE | pv > $DEVICE